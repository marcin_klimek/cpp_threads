#include <iostream>
#include <boost/thread.hpp>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>


int main(int argc, char *argv[])
{
    const size_t SIZE = 500000;

    std::vector<unsigned long long int> v;
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulate:          " << std::accumulate(v.begin(), v.end(), (unsigned long long int)0);
    auto end = std::chrono::high_resolution_clock::now();
    float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " ms" << std::endl;


    //TODO:
    //zaimplementowac wersje rownolegla

    return 0;
}
