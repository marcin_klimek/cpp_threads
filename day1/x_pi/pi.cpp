#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include "boost/thread.hpp"
#include <algorithm>
#include <vector>
#include <algorithm>
#include <random>
#include <stdio.h>


namespace pt = boost::posix_time;


int lrand48_r(struct drand48_data *buffer, long int *result);
int srand48_r(long int seedval, struct drand48_data *buffer);


class RandGen
{
    int range_;

public:

    RandGen(int range) : range_(range)
    {
    }

    ~RandGen()
    {
    }

    int operator()() const
    {
        return rand();
    }
};

double darts(int num_darts)
{
    RandGen rg(RAND_MAX);

    int i, p=0;
    for (i = 1; i <= num_darts; i++)
    {
        double x, y;
        x = static_cast< double >( rg() )/RAND_MAX;
        y = static_cast< double >( rg() )/RAND_MAX;
        if ( (x*x + y*y) <= 1.0)
            p++;
    }
    return 4.0*static_cast< double >( p )/num_darts;
}


void calculate_serial(int num_darts)
{
    pt::ptime start = pt::microsec_clock::local_time();

    double PI = darts(num_darts);
    pt::ptime stop = pt::microsec_clock::local_time();

    std::cout << "For " << num_darts << " darts thrown the value of PI is " << PI << " time " << (stop - start).total_nanoseconds() << std::endl;
}


void calculate_threaded(int num_darts)
{

}

int main()
{
    int num_darts = 100000;

    srand( (u_int)time(NULL) );

    calculate_serial(num_darts);

    //TODO:
    calculate_threaded(num_darts);
}
