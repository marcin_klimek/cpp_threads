#include <iostream>
#include <boost/thread.hpp>
#include <vector>
#include <algorithm>
#include <chrono>

template <typename It, typename T>
void partial_accumulate(It first, It last, T& result)
{
    result = std::accumulate(first, last, T());
}

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned hardware_threads_count = boost::thread::hardware_concurrency();

    if (hardware_threads_count == 0)
        hardware_threads_count = 1;

    std::vector<boost::thread> thds(hardware_threads_count-1);
    std::vector<T> partial_results(hardware_threads_count);

    unsigned count = std::distance(first, last);
    unsigned block_size = count / hardware_threads_count;

    It block_start = first;
    for(unsigned i = 0; i < hardware_threads_count - 1; ++i)
    {
        It block_end = block_start;
        std::advance(block_end, block_size);

        thds[i] = boost::thread(&partial_accumulate<It, T>, block_start, block_end,
                                boost::ref(partial_results[i]));
        block_start = block_end;
    }

    partial_accumulate(block_start, last, partial_results[hardware_threads_count-1]);

    for(auto& t : thds)
        t.join();

    return std::accumulate(partial_results.begin(), partial_results.end(), init);
}

int main(int argc, char *argv[])
{
    std::cout << "Hardware threads: "  << boost::thread::hardware_concurrency() << std::endl;
    const size_t SIZE = 5000000;
    std::vector<long> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v[i] = i;
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulate:          " << std::accumulate(v.begin(), v.end(), 0ll);
    auto end = std::chrono::high_resolution_clock::now();
    float seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
    std::cout << "   " << seconds << " ns" << std::endl;

    std::vector<long> v2(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v2[i] = i;
    }

    start = std::chrono::high_resolution_clock::now();
    std::cout << "Parallel Accumulate: " << parallel_accumulate(v2.begin(), v2.end(), 0ll);
    end = std::chrono::high_resolution_clock::now();
    seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
    std::cout << "   " << seconds << " ns" << std::endl;
    return 0;
}
