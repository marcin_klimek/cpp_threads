#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include "boost/thread.hpp"
#include <algorithm>
#include <vector>
#include <algorithm>
#include <random>

namespace pt = boost::posix_time;

class RandGen
{
    int range_;
    std::mt19937 *gen;

    std::mt19937 get_generator(unsigned int seed)
    {
        std::minstd_rand0 lc_generator(seed);
        std::uint_least32_t seed_data[std::mt19937::state_size];

        std::generate_n(seed_data, std::mt19937::state_size, std::ref(lc_generator));
        std::seed_seq q(std::begin(seed_data), std::end(seed_data));
        return std::mt19937{q};
    }

public:

    RandGen(int range) : range_(range)
    {
        gen = new std::mt19937(get_generator((u_int)time(NULL)));
    }
    ~RandGen()
    {
        delete gen;
    }


    int operator()() const
    {
        std::uniform_int_distribution<> dis(0, range_);
        return dis(*gen);
    }
};


/*


//int lrand48_r(struct drand48_data *buffer, long int *result);
//int srand48_r(long int seedval, struct drand48_data *buffer);

    RandGen(int range) : range_(range)
    {
        int resutl = srand48_r( (long int) time(NULL), &buffer);
    }

    ~RandGen()
    {
    }

    int operator()() const
    {
        long int result;
        lrand48_r(const_cast<drand48_data*>(&buffer), &result);

        return result;
    }
*/

double darts(int num_darts)
{
    RandGen rg(RAND_MAX);

    int i, p=0;
    for (i = 1; i <= num_darts; i++)
    {
        double x, y;
        x = static_cast< double >( rg() )/RAND_MAX;
        y = static_cast< double >( rg() )/RAND_MAX;
        if ( (x*x + y*y) <= 1.0)
            p++;
    }
    return 4.0*static_cast< double >( p )/num_darts;
}

void darts_threaded(int num_darts, int& result)
{
    RandGen rg(RAND_MAX);
    int i, p=0;

    for (i = 1; i <= num_darts; i++)
    {
        double x, y;
        x = static_cast< double >( rg() )/RAND_MAX;
        y = static_cast< double >( rg() )/RAND_MAX;
        if ( (x*x + y*y) <= 1.0)
            p++;
    }

    result = p;
}


void calculate_serial(int num_darts)
{
    pt::ptime start = pt::microsec_clock::local_time();

    double PI = darts(num_darts);
    pt::ptime stop = pt::microsec_clock::local_time();

    std::cout << "For " << num_darts << " darts thrown the value of PI is " << PI << " time " << (stop - start).total_milliseconds() << std::endl;
}

void calculate_threaded(int num_darts)
{
    int num_threads = boost::thread::hardware_concurrency();

    std::vector<int> results( num_threads );
    pt::ptime start = pt::microsec_clock::local_time();

    boost::thread_group grp;

    for(int i=0; i<num_threads; i++)
        grp.create_thread(boost::bind(darts_threaded, num_darts/num_threads, boost::ref(results[i])));

    grp.join_all();

    int result = std::accumulate(results.begin(), results.end(), 0);

    double PI = 4.0*static_cast< double >( result )/num_darts;

    //stop  = omp_get_wtime();
    pt::ptime stop = pt::microsec_clock::local_time();
    std::cout << "For " << num_darts << " darts thrown the value of PI is " << PI << " time " << (stop - start).total_milliseconds() << std::endl;
}


int main()
{
    int num_darts = 300000;

    calculate_serial(num_darts);
    calculate_threaded(num_darts);
}
