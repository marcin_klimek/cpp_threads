#include <iostream>
#include <boost/thread.hpp>

void hello()
{
	std::cout << "Hello Concurrent world!" << std::endl;
}

int main()
{
	boost::thread t1(hello);
	boost::thread t2(hello);

	t1.join();
	t2.join();

}

