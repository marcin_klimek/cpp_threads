#include "net/tcp_socket.h"
#include "thread/launch_thread.h"
#include <iostream>
#include <list>
#include <algorithm>
#include <errno.h>
#include <cstring>
#include <sys/epoll.h>
#include <fcntl.h>
#include <memory>
#include <unistd.h>




class reactor;
class channel;

class event
{
    friend class reactor;

public:
    event(int eventfd) : fd(eventfd)
    {
        memset(&ep_event, 0, sizeof(ep_event));
        ep_event.data.fd = fd;
        ep_event.events = EPOLLIN | EPOLLET;
    }

private:
    int fd;
    epoll_event ep_event;
};



typedef std::function<void()> listener;
typedef std::pair< std::shared_ptr<event>, listener> event_handler;
typedef std::vector<event_handler>::iterator ev_handle_iter;


class reactor
{



public:
     reactor() : reactor_event( epoll_create(1) ), total_events(0)
     {
     }

     ~reactor()
     {
         /*
          * ev_handle_iter it=events_list.begin();
         ev_handle_iter itEnd=events_list.end();
         for(; it!=itEnd;)
         {
             delete it->first;
             ++it;
         }
         */

     }

     void add_event_handler(std::shared_ptr<event> ev, const listener &lst)
     {

         if(epoll_ctl(reactor_event.fd, EPOLL_CTL_ADD, ev->fd, &ev->ep_event)!=0)
         {
             perror("epoll_ctl");
             return;
         }

         events_list.push_back( std::make_pair( std::make_shared<event>(ev->fd), lst) );
         ++total_events;

     }

    void remove_event_handler(event& ev)
    {
        if(epoll_ctl(reactor_event.fd, EPOLL_CTL_DEL, ev.fd, &ev.ep_event)!=0)
        {
            perror("epoll_ctl");
            return;
        }

        auto it=events_list.begin();
        auto itEnd=events_list.end();
        while( it != itEnd )
        {
            std::shared_ptr<event> eptr = it->first;

            if(eptr->fd == ev.fd)
            {
                it = events_list.erase(it);
                break;
            }
            else
            {
                ++it;
            }
        }
        --total_events;
    }

    bool dispatch_events(int timeout=-1)
    {
        memset( events, 0, sizeof(events) );
        int n = epoll_wait(reactor_event.fd, events, MAX_EVENTS, timeout);

        if(!n)
            return false;

        for(int i=0; i<n; ++i)
        {
            event ev( events[i].data.fd );

            if((events[i].events & EPOLLERR) ||
               (events[i].events & EPOLLHUP) ||
              !(events[i].events & EPOLLIN))
            {
                fprintf(stderr, "epoll error\n");
                remove_event_handler( ev );
                continue;
            }

            //std::vector<EventHandler>::iterator
            auto it=std::find_if(events_list.begin(),
                                 events_list.end(),
                                 [&ev](const event_handler &pr)->bool{ return pr.first->fd==ev.fd; } );

            if ( it != events_list.end() )
            {
                listener lst = it->second;
                lst();
            }
        }

        return true;
    }

private:

    event reactor_event;

    std::vector<event_handler> events_list;
    size_t total_events;

    static const unsigned int MAX_EVENTS = 128;
    epoll_event events[MAX_EVENTS];
};


class channel
{
    std::unique_ptr<net::tcp_socket> session_socket;

public:

    channel(net::tcp_socket::sockhandle handle)
    {
        session_socket = std::unique_ptr<net::tcp_socket>( new net::tcp_socket(handle) );
    }

    ~channel()
    {
        session_socket->disconnect();
    }

    void on_read()
    {
        char buf[1024];
        memset(buf, 0, sizeof(buf));
        int num_received = session_socket->receive( buf, sizeof(buf));
        if ( num_received > 0 )
        {
            std::cout << "on_read " << num_received;

            std::transform(buf, buf+num_received, buf, ::toupper);

            int num_send= session_socket->send( buf, num_received+1);
            std::cout << " -> " << num_send<< std::endl;
        }
    }

};

class server
{
    reactor r;
    net::tcp_socket server_socket;

    bool quit;

public:
    server() : quit(false)
    {
    }

    ~server()
    {
        close( server_socket.getfd() );
    }

    void on_incoming_connection()
    {
        net::tcp_socket::sockhandle handle = server_socket.accept();

        std::cout << "on_incoming_connection " << handle <<std::endl;

        channel* ch = new channel( handle );
        r.add_event_handler( std::make_shared<event>(handle), std::bind( &channel::on_read, ch ));

    }

    void on_stdin()
    {
        char buf[1024];
        memset(buf, 0, 1024);
        int num = read(0, buf, 1024);

        std::cout << quit << " " << num << " " << buf;

        std::string s(buf);
        if ( s.compare(0, 4, "quit") == 0 )
            quit=true;
    }

    bool setup()
    {
        try
        {
            server_socket.bind( "127.0.0.1", 12345);
            server_socket.listen();

            std::cout << "server socket: " << server_socket.getfd() << std::endl;
        }
        catch( const net::socket_exception& e)
        {
            std::cerr << e.what() << std::endl;
            return false;
        }

        r.add_event_handler( std::make_shared<event>(server_socket.getfd()), std::bind(&server::on_incoming_connection, this) );
        r.add_event_handler( std::make_shared<event>(fileno(stdin)) , std::bind( &server::on_stdin, this ));

        return true;
    }

    void run()
    {
        while( !quit )
            r.dispatch_events();
    }


};

int main()
{
    server s;

    if ( s.setup() )
        s.run();

    return 0;
}

