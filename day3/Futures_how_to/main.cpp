#include <iostream>
#include <pthread.h>
#include "futures.h"
#include <memory>
#include <unistd.h>


int answer()
{
    sleep(1);
    return 42;
}

int main()
{
    std::cout << "Hello World!" << std::endl;

    future<int> res = async<int>(&answer );
    future<int> res2 = async<int>( []() -> int {return 6;} );

    std::cout << res.get() << std::endl;
    std::cout << res2.get() << std::endl;

    return 0;
}

