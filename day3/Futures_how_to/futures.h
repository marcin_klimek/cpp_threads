#ifndef FUTURES_H
#define FUTURES_H
#include <iostream>
#include <functional>
#include <memory>
#include <thread>

template <typename T>
class ptask
{
    T& result;

    std::function<T()> task;

public:

    ptask( std::function<T()> t, T& r ) : result(r), task(t)
    {
    }

    void operator() ()
    {
        std::cout << "ptask - start" << std::endl;

        result = task();

        std::cout << "ptask - end" << std::endl;
    }
};


template <typename T>
class future
{
    T result;

    ptask<T> task;
    std::unique_ptr<std::thread> worker;

public:

    future(const future & ) = delete;           // no copying
    void operator=( const future& ) = delete;   // no copying

    future( future&& other ) : task(std::move(other.task)), result(std::move( other.result )), worker(std::move( other.worker ))
    {
    }

    future( std::function<T()> t) : task( ptask<T>(t, result) )
    {
        worker = std::unique_ptr<std::thread>( new std::thread( task ));
    }

    ~future()
    {
        if(worker->joinable())
            worker->join();
    }

    T get()
    {
        if(worker->joinable())
            worker->join();

        return result;
    }

};


template <typename T>
future<T> async( std::function<T()> f )
{
    return future<T>(f);
}


#endif // FUTURES_H
