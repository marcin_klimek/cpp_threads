#include <iostream>
#include <thread>
#include <future>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned hardware_threads_count = std::thread::hardware_concurrency();

    if (hardware_threads_count == 0)
        hardware_threads_count = 1;

    std::vector<std::future<T>> results;

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_threads_count;

    It range_start = first;
    for(unsigned i = 0; i < hardware_threads_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        results.push_back(std::async(std::launch::async,
                                     std::bind(&std::accumulate<It, T>,
                                             range_start, range_end, T())));
        range_start = range_end;
    }   

    T result = std::accumulate(range_start, last, init);

    for(auto& r : results)
        result += r.get();

    return result;
}

int main(int argc, char *argv[])
{
    std::cout << "Hardware threads: "  << std::thread::hardware_concurrency() << std::endl;
    const size_t SIZE = 50000000;
    std::vector<long> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulate:          " << std::accumulate(v.begin(), v.end(), 0);
    auto end = std::chrono::high_resolution_clock::now();
    float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " ms" << std::endl;

    std::vector<long> v2(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v2.push_back(i);
    }

    start = std::chrono::high_resolution_clock::now();
    std::cout << "Parallel Accumulate: " << parallel_accumulate(v2.begin(), v2.end(), 0);
    end = std::chrono::high_resolution_clock::now();
    seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " ms" << std::endl;
    return 0;
}
