#include <iostream>
#include <thread>
#include <mutex>
#include <future>
#include <vector>
#include "thread_safe_queue.hpp"

using namespace std;

class BankAccount
{
    const int id_;
    double balance_ = 0.0;
public:
    BankAccount(int id) : id_{id}
    {}

    void withdraw(double amount)
    {
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        return balance_;
    }
};

//C++11 wrapper
template <typename T>
class Wrapper
{
private:
    T wrapped_;
public:
    Wrapper(T wrapped = T{}) : wrapped_{wrapped}
    {}

    template <typename F>
    auto operator()(F f) -> decltype(f(wrapped_))
    {
        return f(wrapped_);
    }
};

//C++11 monitor
template <typename T>
class Monitor
{
private:
    mutable T wrapped_;
    mutable std::mutex mtx_;
public:
    Monitor(T wrapped = T{}) : wrapped_(wrapped)
    {}

    template <typename F>
    auto operator()(F f) const -> decltype(f(wrapped_))
    {
        std::lock_guard<std::mutex> lk{mtx_};
        return f(wrapped_);
    }
};


template<class T>
class Concurrent
{
private:
    mutable T t;
    mutable ThreadSafeQueue<std::function<void()>> q;
    bool done = false;
    std::thread thd;

    void run()
    {
        while(!done)
        {
            std::function<void()> f;
            q.wait_and_pop(f);
            f();
        }
    }

public:
    Concurrent( T t_)
        : t{t_}, thd([this] { run(); })
    {}

    ~Concurrent()
    {
        q.push([=]{ done=true; });
        thd.join();
    }

    template<typename F>
    void operator()(F f) const
    {
        q.push([=]{ f(t); });
    }
};


int main()
{
    Monitor<BankAccount> w { BankAccount(1) };

    w([](BankAccount& ba) {
        ba.withdraw(100);
        ba.deposit(20);
        cout << ba.balance() << endl;
    });

    Monitor<string> s{"start\n"};

    vector<future<void>> v1;

    for(int i = 0; i < 5; ++i)
    {
        v1.push_back(async([&, i] {
            s([=](string& s) {
                s += to_string(i) + " " + to_string(i);
                s += "\n";
            });
            s([](string& s) {
                cout << s;
            });
        }));
    }

    for(auto& f : v1)
        f.wait();

    cout << "Done." << endl;

    cout << "\n\n";

    // monitor of cout
    Monitor<ostream&> sync_cout{cout};

    vector<future<void>> v2;
    for(int i = 0; i < 5; ++i)
    {
        v2.push_back(async([&, i] {
            sync_cout([=](ostream& cout) {
                cout << to_string(i) << " " << to_string(i);
                cout << "\n";
            });
            sync_cout([=](ostream& cout) {
                cout << "Hi from " <<  i << endl;
            });
        }));
    }

    for(auto& f : v2)
        f.wait();

    sync_cout([](ostream& cout) { cout << "Done." << endl;});


    cout << "\n\n";

//    {
//        Concurrent<string> s{"start\n"};

//        vector<future<void>> v3;

//        for(int i = 0; i < 5; ++i)
//        {
//            v3.push_back(async([&, i] {
//                s([=](string& s) {
//                    s += to_string(i) + " " + to_string(i);
//                    s += "\n";
//                });
//                s([](string& s) {
//                    cout << s;
//                });
//            }));
//        }

//        for(auto& f : v1)
//            f.wait();

//        cout << "Done." << endl;

//    }
}
