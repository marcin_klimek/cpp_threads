#include <iostream>
#include <fstream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>
#include "active_object.hpp"

using namespace std;

class Logger    {
    ofstream fout_;
    ActiveObject ao_;

public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    void log(const string& message)
    {
        ao_.send([message, this] { do_log(message); });
    }

    ~Logger()
    {
        ao_.send([=] { fout_.close(); });
    }
private:
    void do_log(const string& message)
    {
        fout_ << message << endl;
    }
};

using Logger;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + boost::lexical_cast<string>(id) + " - Event#" + boost::lexical_cast<string>(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    boost::thread thd1(&run, boost::ref(log), 1);
    boost::thread thd2(&run, boost::ref(log), 2);

    thd1.join();
    thd2.join();
}

