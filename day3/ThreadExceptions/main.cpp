#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/exception_ptr.hpp>

using namespace std;

void background_work(int id, int count, boost::exception_ptr& excpt)
{
    try
    {
        for(int i = 1; i <= count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));

            if (i == 13)
                throw std::runtime_error("Error in THD# " + to_string(id));
        }
    } catch(...)
    {
        cout << "Catching exception..." << endl;
        excpt = boost::current_exception();
    }
}

int main()
{
    const int NO_OF_THREADS = 8;

    boost::thread_group thd_group;
    vector<boost::exception_ptr> vec_excpts(8);

    for(int i = 0; i < NO_OF_THREADS; ++i)
    {
        boost::thread* pthd
                = new boost::thread(
                    boost::bind(&background_work, i+1, i % 2 ? 15 : 5,
                                boost::ref(vec_excpts[i])));
        thd_group.add_thread(pthd);
    }

    thd_group.join_all();

    // obsluga bledu
    for(auto e : vec_excpts)
    {
        if (e)
        {
            try
            {
                boost::rethrow_exception(e);
            }
            catch(const std::runtime_error& e)
            {
                cout << "Main catched exception: " << e.what() << endl;
            }
        }
    }
}
