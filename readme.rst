login haslo do VM
-----------------

   ``info  /  info``


reinstalacja dodatków do VBox
-----------------------------

``sudo /etc/init.d/vboxadd setup``


ustawienie proxy
----------------

Najwygodniej jest dodac wpis w pliku .profile

export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080


ustawienie proxy dla APT
------------------------

/etc/apt/apt.conf

Acquire::http::proxy "http://10.144.1.10:8080/";
Acquire::https::proxy "https://10.144.1.10:8080/";
Acquire::ftp::proxy "ftp://10.144.1.10:8080/";


test polaczenia
----------------

ping 8.8.8.8 # przy zalozeniu, ze ICMP nie jest blokowane
curl -v http://www.example.com/


git
---

git-cheat-sheet
http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

git status  # status lokalnego repozytorium
git pull    # pobranie plikow z repozytorium
git stash   # przeniesienie modyfikacji do schowka


linki
-----

sortowanie i branch prediction:
http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array

what every programmer should know about memory:
http://lwn.net/Articles/250967/

http://accu.org/content/conf2014/Howard_Hinnant_Accu_2014.pdf
http://channel9.msdn.com/Shows/Going+Deep/Cpp-and-Beyond-2012-Herb-Sutter-atomic-Weapons-1-of-2
http://channel9.msdn.com/Shows/Going+Deep/Cpp-and-Beyond-2012-Herb-Sutter-atomic-Weapons-1-of-2
http://www.italiancpp.org/wp-content/uploads/2014/03/CppISO-Feb2014-r1.pdf
https://onedrive.live.com/view.aspx?resid=F1B8FF18A2AEC5C5!1176&cid=f1b8ff18a2aec5c5&app=WordPdf
http://preshing.com/
http://www.1024cores.net/
http://herbsutter.com/
http://bartoszmilewski.com/
http://www.viva64.com/en/b/

kompilacja
----------

g++ main.cpp -o prg -lpthread -std=c++11
./prg


cmake
-----

  jeśli jestesmy w katalogu, gdzie znajduje się CMakeLists.txt

    mkdir build
    cd build
    cmake ..
    make

    ./<nasz program>


mudflap -> adress sanitizer pod GCC od 4.8
------------------------------------------

-fsanitize=address -fno-omit-frame-pointer
