#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

boost::timed_mutex mtx;

void background_worker(int id, int timeout)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    boost::unique_lock<boost::timed_mutex> lk(mtx, boost::try_to_lock);

    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD#" << id << " doesn't own a lock... Tries to acquire a mutex..." << endl;
        } while (!lk.try_lock_for(boost::chrono::seconds(timeout)));
    }

    cout << "Start of THD#" << id << endl;

    boost::this_thread::sleep_for(boost::chrono::seconds(10));

    cout << "End of THD#" << id << endl;
}

int main()
{
    boost::thread thd1(&background_worker, 1, 2);
    boost::thread thd2(&background_worker, 2, 1);

    thd1.join();
    thd2.join();
}
