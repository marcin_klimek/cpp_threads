#include <iostream>
#include <boost/thread.hpp>
#include <vector>
#include <atomic>

const size_t size = 20;

std::vector<int> data(size);


boost::shared_mutex mtx;
boost::mutex cout_mtx;

std::atomic<int> writers_counter;
boost::mutex writer_mtx;
boost::condition_variable cond;

void init_data(std::vector<int> &data, int size)
{
    for(size_t i = 0; i < size; ++i)
        data[i] = i + 1;
}

void reader(int id)
{
    while(true)
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(id*300));
        size_t index = 0;//(i + id*2) % size;

        if (writers_counter > 0)
        {
            std::cout << "reader needs to wait - writers in queue " << writers_counter << std::endl;

            boost::unique_lock<boost::mutex> lk(writer_mtx);
            while ( writers_counter > 0 )
            {
                cond.wait(lk);
                std::cout << "after wait " << writers_counter << std::endl;
            }
        }

        std::cout << "Reader before shared lock" << std::endl;
        boost::shared_lock<boost::shared_mutex> lk(mtx);
        int value = data[index];

        // processing data
        std::cout << "Thread#" << id << " reads slot at " << index << " : " << value << std::endl;
    }
}

void writer(int id)
{
    while(true)
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(1000+id*300));

        std::cout << "Writer Before lock guard " << writers_counter << std::endl;

        {
            if ( writers_counter == 0 )
                writers_counter=1;

            //writers_counter++;
            boost::lock_guard<boost::shared_mutex> lk(mtx);

            // writing data
            //data[i] = 0;

            std::cout << "Writer thread#" << id << " writes" << std::endl;
            boost::this_thread::sleep_for(boost::chrono::milliseconds(2000 +id*300));

            std::cout << "Writer thread#" << id << " done " << writers_counter << std::endl;
            //done
            writers_counter = 0;
            //writers_counter--;
        }
        cond.notify_all();
        std::cout << "Writer thread#" << id << " after notify " << writers_counter << std::endl;
    }
}

int main()
{
    init_data(data, size);

    boost::thread_group thd_group;

    thd_group.create_thread(boost::bind(&reader, 1));
    thd_group.create_thread(boost::bind(&reader, 2));
    thd_group.create_thread(boost::bind(&reader, 3));
    thd_group.create_thread(boost::bind(&writer, 4));

    thd_group.join_all();
}
