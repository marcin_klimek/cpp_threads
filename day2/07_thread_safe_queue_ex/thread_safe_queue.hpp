#include <boost/thread.hpp>
#include <queue>

template <typename T>
class ThreadSafeQueue
{
private:
    mutable boost::mutex mtx_;
    std::queue<T> queue_;
    boost::condition_variable condition_;
    
public:
    ThreadSafeQueue()
    {
    }
    
    ThreadSafeQueue(const ThreadSafeQueue& other)
    {
        boost::lock_guard< boost::mutex > lk( other.mtx_ );
        queue_ = other.queue_;
    }
    
    void push(T value)
    {
        boost::lock_guard< boost::mutex > lk( mtx_ );
        queue_.push( value);

        condition_.notify_one();
    }
    

    
    boost::shared_ptr<T> pop()
    {
        boost::unique_lock< boost::mutex > lk(mtx_);
        
        condition_.wait(lk, !boost::bind( &std::queue<T>::empty, boost::ref( queue_ ) ) );
        boost::shared_ptr<T> result( new T( queue_.front() ) );
        queue_.pop();

        return result;
    }
    void pop(T& value)
    {
        boost::unique_lock< boost::mutex > lk(mtx_);

        condition_.wait(lk, !boost::bind( &std::queue<T>::empty, boost::ref( queue_ ) ) );
        value = queue_.front();
        queue_.pop();
    }

    bool try_pop(T& value)
    {
        boost::lock_guard< boost::mutex > lk( mtx_ );

        if (queue_.empty())
            return false;

        value = queue_.front();
        queue_.pop();

        return true;
    }
    
    bool is_empty()
    {
		boost::lock_guard< boost::mutex > lk( mtx_ );
    	return queue_.empty();
    }

};




