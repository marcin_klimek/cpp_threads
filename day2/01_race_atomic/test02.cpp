#include <atomic>

std::atomic<unsigned long long int> sharedValue(0);

void storeValue()
{
    sharedValue.store(0x100000002, std::memory_order_relaxed);
}

unsigned long long int loadValue()
{
    return sharedValue.load(std::memory_order_relaxed);
}
