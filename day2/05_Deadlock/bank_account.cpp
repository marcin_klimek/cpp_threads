#include <boost/thread.hpp>
#include <boost/thread/lockable_adapter.hpp>
#include <iostream>

class BankAccount
{
    const int id_;
    double balance_;
    mutable boost::mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account " << id_ << std::endl;
        boost::lock_guard<boost::mutex> lk(mtx_);
        std::cout << "balance = " << balance_ << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        boost::unique_lock<boost::mutex> lk_from(mtx_, boost::defer_lock);
        boost::unique_lock<boost::mutex> lk_to(to.mtx_, boost::defer_lock);

        boost::lock(lk_from, lk_to);

        balance_ -= amount;
        to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        return balance_;
    }
};


void make_transfers(BankAccount& ba1, BankAccount& ba2, int no_of_operations, int thd_id)
{
    for(int i = 0; i < no_of_operations; ++i)
    {
        std::cout << "THD#" << thd_id
             << " transfer from ba#" << ba1.id()
             << " to ba#" << ba2.id() << std::endl;

        ba1.transfer(ba2, 1.0);
    }
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    boost::thread thd1(&make_transfers, boost::ref(ba1), boost::ref(ba2),
                       10000, 1);
    boost::thread thd2(&make_transfers, boost::ref(ba2), boost::ref(ba1),
                       10000, 2);

    thd1.join();
    thd2.join();

    std::cout << "Po przelewach:\n";
    ba1.print();
    ba2.print();
}
